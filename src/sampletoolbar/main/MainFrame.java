package sampletoolbar.main;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import sampletoolbar.component.AppJToolbar;

public class MainFrame extends JFrame {
	
	public MainFrame() {
		super("ToolbarSample");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		JPanel pane = (JPanel) this.getContentPane();
		pane.add(new AppJToolbar(), BorderLayout.NORTH);
	}

}