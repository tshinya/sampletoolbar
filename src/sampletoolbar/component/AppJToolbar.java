package sampletoolbar.component;

import javax.swing.JToolBar;

public class AppJToolbar extends JToolBar {

	public AppJToolbar() {
		super();
		this.add(new OpenFileJButton());
		this.add(new OpenCSVJButton());
	}
}
